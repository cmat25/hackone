import React from "react";
import {useDropzone} from "react-dropzone";
import './ImageDropzone.css'

type ImageDropzoneProps = {
  onDrop: (imageFile: File) => void
}

export function ImageDropzone(props: ImageDropzoneProps) {
  const {acceptedFiles, getRootProps, getInputProps} = useDropzone({
    accept: 'image/*',
    multiple: false,
    onDropAccepted: files => files.map(file => props.onDrop(file))
  })

  const prompt = <p>Drag and drop a file here, or click to browse</p>
  const files = acceptedFiles.map(file => (
      <li key={file.name}>
        {file.name} - {file.size} bytes
      </li>
  ))

  return (
      <section>
        <div {...getRootProps({className: 'dropzone'})} data-test="dropzone">
          <input {...getInputProps()} />
          {files.length > 0 ? files : prompt}
        </div>
      </section>
  )
}