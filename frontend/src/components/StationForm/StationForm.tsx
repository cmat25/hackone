import React from 'react'
import { TextField, Grid, Select, FormControl, InputLabel, InputAdornment, FormHelperText, Button } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';

import './StationForm.css';

export default class StationForm extends React.Component {
  render() {
    return (
      <div>
        <h1>
          Station
        </h1>

        <Grid container>
          <Grid item xs={4}>
          </Grid>

          <Grid item xs={4}>
            <form className="station-form" noValidate autoComplete="off">
              <FormControl variant="outlined" className="input-field">
                <InputLabel htmlFor="station">Age</InputLabel>
                <Select
                  native
                  label="Station"
                  inputProps={{
                    name: 'station',
                    id: 'station',
                  }}
                >
                  <option aria-label="None" value="" />
                  <option value={1}>A</option>
                  <option value={2}>B</option>
                </Select>
              </FormControl>

              <FormControl variant="outlined" className="input-field">
                <InputLabel htmlFor="title">Title</InputLabel>
                <OutlinedInput id="title" label="Title"
                  endAdornment={
                    <InputAdornment position="start">
                      <Visibility />
                    </InputAdornment>
                  }
                />
                <FormHelperText id="title">The name of the image. Please use lower case. *Count</FormHelperText>
              </FormControl>
              
              <FormControl variant="outlined" className="input-field">
                <InputLabel htmlFor="alt">Alt</InputLabel>
                <OutlinedInput id="alt" label="Alt"
                  endAdornment={
                    <InputAdornment position="start">
                      <Visibility />
                    </InputAdornment>
                  }
                />
                <FormHelperText id="title">Should describe the station in less than 125 characters. *Count</FormHelperText>
              </FormControl>

              <FormControl variant="outlined" className="input-field">
                <InputLabel htmlFor="city">City</InputLabel>
                <OutlinedInput id="city" label="City"
                  endAdornment={
                    <InputAdornment position="start">
                      <Visibility />
                    </InputAdornment>
                  }
                />
              </FormControl>

              <FormControl variant="outlined" className="input-field">
                <InputLabel htmlFor="type">Type</InputLabel>
                <Select
                  native
                  label="Type"
                  inputProps={{
                    name: 'type',
                    id: 'type',
                  }}
                >
                  <option aria-label="None" value="" />
                  <option value={1}>A</option>
                  <option value={2}>B</option>
                </Select>
              </FormControl>

              <Button variant="contained" color="primary"  className="input-field">
                Submit
              </Button>
            </form>
          </Grid>

          <Grid item xs={4}>
          </Grid>
        </Grid>
      </div>
      
    );
  }
};