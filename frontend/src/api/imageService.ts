import {baseUrl} from "./baseUrl";

/**
 * Uploads the provided file to our Google Cloud bucket.
 */
export async function uploadImage(imageFile: File): Promise<void> {
  const uploadUrl = await getUploadUrl(imageFile.name)
  console.log(`uploadImage: uploadImage - ${uploadUrl}`)
  return await postImage(uploadUrl, imageFile)
}

async function getUploadUrl(filename: string): Promise<string> {
  const url = new URL('upload', baseUrl)
  url.searchParams.set('filename', filename)

  const response = await fetch(url.href)

  if (!response.ok) {
    throw new Error(await response.text())
  }
  return await response.text()
}

async function postImage(uploadUrl: string, imageFile: File): Promise<void> {
  const response = await fetch(uploadUrl, {
    method: 'PUT',
    body: imageFile,
    headers: {
      "Content-Type": imageFile.type
    },
  })

  if (!response.ok) {
    console.error(`Failed to upload image: ${imageFile.name}`)
    throw new Error(await response.text())
  }
  console.log(`Image uploaded successfully: ${imageFile.name}`)
}