import React from 'react'
import StationForm from './components/StationForm/StationForm';

import './App.css'

function App() {
  return (
    <div className="App">
      <header className="App-header">
      </header>
      <StationForm></StationForm>
    </div>
  )
}

export default App
