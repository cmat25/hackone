#!/usr/bin/env bash

# *****************************************************************************
# WARNING: This will clear the database and re-seed it.
# Usage: ./reset-database {root_password}
# *****************************************************************************
mysql --host 35.224.122.74 --user=root --password=$1 < sql/init.sql
mysql --host 35.224.122.74 --user=root --password=$1 content < sql/schema.sql
mysql --host 35.224.122.74 --user=root --password=$1 content < sql/triggers.sql
mysql --host 35.224.122.74 --user=root --password=$1 content < sql/seed/country.sql
mysql --host 35.224.122.74 --user=root --password=$1 content < sql/seed/transit_image.sql
