#!/usr/bin/env bash
# Login/Auth to gcloud
gcloud auth login
gcloud config set project content-tools-248505

# Setup run config
gcloud config set run/platform managed
gcloud config set run/region us-central1

# Deploy the application
gcloud builds submit --config cloudbuild.yaml .
gcloud beta run deploy content-services --image gcr.io/content-tools-248505/content-services --platform managed

# Setting up Endpoints
gcloud beta run deploy api-gateway \
    --image="gcr.io/endpoints-release/endpoints-runtime-serverless:1.30.0" \
    --allow-unauthenticated \
    --project=content-tools-248505

gcloud endpoints services deploy openapi.yaml \
  --project content-tools-248505

gcloud beta run services update api-gateway \
   --set-env-vars ENDPOINTS_SERVICE_NAME=api-gateway-ik44eed7tq-uc.a.run.app \
   --project content-tools-248505

cloud beta run services add-iam-policy-binding content-services \
    --member "serviceAccount:534108375123-compute@developer.gserviceaccount.com" \
    --role "roles/run.invoker" \
    --project content-tools-248505

# Cloud SQL
# You must configure the correct environment variables on the production container
gcloud beta run services update content-services --add-cloudsql-instances content-database
gcloud sql users set-password root % --instance content-database --password ***

# Setting up the DB
# You need to add your public IP to the whitelist and then use the mysql client
# to execute the files
# Find your external IP
dig TXT +short o-o.myaddr.l.google.com @ns1.google.com
# Setup DB
mysql --host 35.224.122.74 --user=root --password=*** < sql/init.sql
mysql --host 35.224.122.74 --user=root --password=*** content < sql/schema.sql
mysql --host 35.224.122.74 --user=root --password=*** content < sql/triggers.sql
mysql --host 35.224.122.74 --user=root --password=*** content < sql/seed/country.sql
mysql --host 35.224.122.74 --user=root --password=*** content < sql/seed/transit_images.sql

# WIP Trying to configure ESP to run on local so we have the exact same environment

#gcloud iam service-accounts create esp-local \
#  --display-name "ESP Local"
#
#// esp-local@content-tools-248505.iam.gserviceaccount.com
#
#gcloud iam service-accounts keys create service-account-creds.json \
#  --iam-account esp-local@content-tools-248505.iam.gserviceaccount.com
#
#gcloud projects add-iam-policy-binding content-tools-248505 \
#    --member serviceAccount:esp-local@content-tools-248505.iam.gserviceaccount.com \
#    --role roles/servicemanagement.serviceController
#
#gcloud projects add-iam-policy-binding content-tools-248505 \
#    --member serviceAccount:esp-local@content-tools-248505.iam.gserviceaccount.com \
#    --role roles/cloudtrace.agent

gcloud projects add-iam-policy-binding content-tools-248505 \
  --member serviceAccount:transit-image-builder@content-tools-248505.iam.gserviceaccount.com \
  --role roles/serviceAccountTokenCreator
#
#sudo docker run --name="esp-test" --publish=8082:8082 --volume=$PWD/esp:/esp gcr.io/endpoints-release/endpoints-runtime:1 --service=service=api-gateway-mcjbodpcia-uc.a.run.app --rollout_strategy=managed --http_port=8082 --backend=docker.for.mac.localhost:8080 --service_account_key=/esp/esp-service-account.json
