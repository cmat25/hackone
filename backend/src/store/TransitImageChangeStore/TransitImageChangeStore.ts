import SqlDatabase from '../../database/SqlDatabase'
import { TransitImageChangeModel } from './TransitImageChangeModel'

class TransitImageChangeStore {
  database: SqlDatabase

  constructor(database: SqlDatabase) {
    this.database = database
  }

  async changes(since?: string, id?: string, author?: string): Promise<TransitImageChangeModel[]> {
    let sqlQuery = `
        SELECT *
        FROM transit_image_audit
    `

    if (since || id || author) {
      sqlQuery += '\nWHERE (change_timestamp > ?1) OR (id = ?2) OR (change_author = ?3)'
    }

    sqlQuery += '\nORDER BY change_timestamp DESC'

    const rows: TransitImageChangeModel[] = await this.database.executeQuery(sqlQuery, [since, id, author])
    return rows
  }
}

export default TransitImageChangeStore
