// All columns in the transit_image_audit table
import { Position } from '../common/Position'

type TransitImageChangeModel = {
  change_id: number
  change_action: string
  change_author: string
  change_timestamp: string
  id: number
  position: Position
  agency_id: string
  source_url: string
  reference_url: string
  credit: string
  country_code: string
  line_name: string
  transit_kind: string
  is_stop: boolean
  title: string
}

export { TransitImageChangeModel }