// This matches the database definition for the transit_image table.
import { Position } from '../common/Position'

interface TransitImageModel {
  id: number
  position?: Position
  agency_id?: string
  source_url: string
  reference_url: string
  credit: string
  country_code: string
  line_name?: string
  transit_kind?: string
  is_stop: boolean
  title: string
  metadata_last_edited: string
  metadata_last_edited_by: string
  metadata_is_active: boolean
}

// This is the type the database accepts, other fields are updated/created by the db
type InsertTransitImageModel = Omit<TransitImageModel, 'id' | 'metadata_last_edited' | 'metadata_is_active'>

export {
  InsertTransitImageModel,
  TransitImageModel,
}
