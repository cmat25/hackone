import { InsertTransitImageModel, TransitImageModel } from './TransitImageModel'
import SqlDatabase from '../../database/SqlDatabase'

class TransitImageStore {
  database: SqlDatabase

  constructor(database: SqlDatabase) {
    this.database = database
  }

  async getImages(...ids: string[]): Promise<TransitImageModel[]> {
    const sqlQuery = `
        SELECT *
        FROM transit_image
        WHERE id IN (${ids.join()});
    `

    const rows = await this.database.executeQuery(sqlQuery, ids)
    if (rows.length == 0) {
      return undefined
    } else {
      return rows
    }
  }

  async searchImage(query: string, limit?: number, offset?: number): Promise<string[]> {
    let sqlQuery
    if (!query) {
      // Simple case with no query, just get the newest created
      sqlQuery = `SELECT id
                  FROM transit_image
                  WHERE metadata_is_active = TRUE
                  ORDER BY id DESC
      `
    } else {
      sqlQuery = ` SELECT id
                   FROM transit_image
                            LEFT JOIN country ON country_code = country.iso
                   WHERE metadata_is_active = TRUE
                     AND (printable_name LIKE CONCAT('%', ?1, '%')
                       OR title LIKE CONCAT('%', ?1, '%')
                       OR agency_id LIKE CONCAT('%', ?1, '%'))
                   ORDER BY CASE
                                # exact matches first
                                WHEN printable_name LIKE ?1 OR title LIKE ?1 OR agency_id LIKE ?1 THEN 1
                                # Prefix matches second
                                WHEN printable_name LIKE CONCAT(?1, '%') OR title LIKE CONCAT(?1, '%') OR
                                     agency_id LIKE CONCAT(?1, '%') THEN 2
                                ELSE 3
                                END ASC,
                            agency_id ASC,
                            title ASC
      `
    }

    if (limit !== undefined) {
      sqlQuery += `LIMIT ?2 OFFSET ?3;`
    } else {
      sqlQuery += `;`
    }

    const rows = await this.database.executeQuery(sqlQuery, [query, limit, offset !== undefined ? offset : 0])
    return rows.map((it: any) => it.id)
  }

  createImage(image: InsertTransitImageModel): Promise<any> {
    let positionText
    if (image.position) {
      positionText = `POINT(${image.position.lat} ${image.position.lng})`
    }
    return this.database.executeQuery(
        `INSERT INTO transit_image(position, agency_id, line_name, transit_kind, is_stop, title, source_url,
                                   reference_url, credit, country_code,
                                   metadata_last_edited_by)
         VALUES (ST_PointFromText(?1, 4326), ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11)`,
      [positionText, image.agency_id, image.line_name, image.transit_kind, image.is_stop, image.title, image.source_url, image.reference_url, image.credit, image.country_code, image.metadata_last_edited_by],
    )
  }

  deleteImage(imageId: string, editor: string): Promise<void> {
    return this.database.executeQuery(
        `
                UPDATE transit_image
                SET metadata_is_active= FALSE,
                    metadata_last_edited_by=?1
                WHERE (id = ?2);`,
      [
        editor, imageId,
      ])
  }

  updateImage(id: string, image: InsertTransitImageModel): Promise<void> {
    let positionText
    if (image.position) {
      positionText = `POINT(${image.position.lat} ${image.position.lng})`
    }

    return this.database.executeQuery(
        `
                UPDATE transit_image
                SET position=ST_PointFromText(?1, 4326),
                    agency_id=?2,
                    line_name=?3,
                    transit_kind=?4,
                    is_stop=?5,
                    title=?6,
                    source_url=?7,
                    reference_url=?8,
                    credit=?9,
                    country_code=?10,
                    metadata_last_edited_by=?11
                WHERE (id = ?12);`,
      [
        positionText, image.agency_id, image.line_name, image.transit_kind, image.is_stop, image.title, image.source_url, image.reference_url, image.credit, image.country_code, image.metadata_last_edited_by, id,
      ],
    )
  }
}

export default TransitImageStore
