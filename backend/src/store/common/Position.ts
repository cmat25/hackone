type Position = {
  lat: number,
  lng: number
}

export { Position }