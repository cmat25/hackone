class UserStore {

  isAuthorised(email: string): boolean {
    return email.endsWith('@rome2rio.com') || this.users[email] !== undefined
  }

  // Hardcode our users, this will be replaced by an integration with Okta.
  users: {
    [email: string]: User
  } = {
    'transit-image-builder@content-tools-248505.iam.gserviceaccount.com': {
      name: 'Transit Image Builder',
      email: 'transit-image-builder@content-tools-248505.iam.gserviceaccount.com',
    },
    'alisa.tykhonova@gmail.com': { name: 'Alisa Tykhonova', email: 'alisa.tykhonova@gmail.com' },
    'szotyi.ko@hotmail.com': { name: 'Sojeong Ko', email: 'szotyi.ko@hotmail.com' },
    'angeline.ibarra28@gmail.com': { name: 'Angeline Ibarra', email: 'angeline.ibarra28@gmail.com' },
    'oliviawu08@yahoo.com': { name: 'Yanshuo (Olivia) Wu', email: 'oliviawu08@yahoo.com' },
    'yoshizawa2712@gmail.com': { name: 'Hiroko Yoshizawa', email: 'yoshizawa2712@gmail.com' },
    'anhtt265@gmail.com': { name: 'Thai Tuan Anh', email: 'anhtt265@gmail.com' },
    'nakhrustalev@gmail.com': { name: 'Nikolai Khrustalev', email: 'nakhrustalev@gmail.com' },
    'thomas.schultz.wenk@gmail.com': { name: 'Thomas Schultz Wenk', email: 'thomas.schultz.wenk@gmail.com' },
    'mariajieval@gmail.com': { name: 'Maria Vali (Ceci)', email: 'mariajieval@gmail.com' },
    'germanpaparoni@gmail.com': { name: 'Gérman Paparoni', email: 'germanpaparoni@gmail.com' },
    'r2jc2004@gmail.com': { name: 'Ricardo Rodriguez', email: 'r2jc2004@gmail.com' },
    'artenova.mail@gmail.com': { name: 'Ana Fonseca', email: 'artenova.mail@gmail.com' },
    'marisa@rome2rio.com': { name: 'Marisa Tesauro', email: 'marisa@rome2rio.com' },
    'melisa.2707@gmail.com': { name: 'Melisa González', email: 'melisa.2707@gmail.com' },
    'kimrackham@gmail.com': { name: 'Kimberly Rackham', email: 'kimrackham@gmail.com' },
    'kimcaseyfreelance@gmail.com': { name: 'Kim Casey (Felstead)', email: 'kimcaseyfreelance@gmail.com' },
    'felixhiggs@hotmail.com': { name: 'Felix Higgs', email: 'felixhiggs@hotmail.com' },
    'maxandsonya@webmin.com': { name: 'Sonya Cameron', email: 'maxandsonya@webmin.com' },
    'gs0185@gmail.com': { name: 'George Sotos', email: 'gs0185@gmail.com' },
    'skardio1986@gmail.com': { name: 'Dionisis Skarmoutsos', email: 'skardio1986@gmail.com' },
    'sophie.papailiadou@gmail.com': { name: 'Sophie Papailiadou', email: 'sophie.papailiadou@gmail.com' },
    'anthy.kapsalis@gmail.com': { name: 'Anthy Kapsalis', email: 'anthy.kapsalis@gmail.com' },
    'terymusalia@gmail.com': { name: 'Terry Musalia', email: 'terymusalia@gmail.com' },
    'stefanseat@gmail.com': { name: 'Stefan Seat', email: 'stefanseat@gmail.com' },
    'victord@rome2rio.com': { name: 'Victor Derian', email: 'victord@rome2rio.com' },
    'lifetrix@gmail.com': { name: 'Ankit Agarwal', email: 'lifetrix@gmail.com' },
    'river49d@gmail.com': { name: 'River Ferdinando', email: 'river49d@gmail.com' },
    'melania.saponaro@gmail.com': { name: 'Melania Sapanoro', email: 'melania.saponaro@gmail.com' },
    'alvaro.suarez.trabanco@gmail.com': { name: 'Alvaro Trabanco', email: 'alvaro.suarez.trabanco@gmail.com' },
  }
}

type User = {
  name: string
  email: string
}

export { UserStore }