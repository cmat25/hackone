import { Storage } from '@google-cloud/storage'
import { ImageStore } from './ImageStore'

const storage = new Storage()

const BUCKET_NAME = 'temporary-image-store'

const temporaryImageBucket = storage.bucket(BUCKET_NAME)

class GoogleCloudImageStore implements ImageStore {
  async getUploadUrl(filename: string): Promise<string> {
    const response = await temporaryImageBucket.file('somefile').getSignedUrl({
      version: 'v4',
      action: 'write',
      expires: Date.now() + 900000,
    })
    return response[0]
  }
}

export { GoogleCloudImageStore }
