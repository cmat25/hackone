interface ImageStore {
  getUploadUrl(filename: string): Promise<string>
}

export { ImageStore }
