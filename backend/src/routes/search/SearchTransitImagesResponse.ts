type SearchTransitImagesResponse = {
  transitImagesIds: string[]
}

export { SearchTransitImagesResponse }
