import * as Router from 'koa-router'
import TransitImageStore from '../../store/TransitImageStore/TransitImageStore'
import { database } from '../../database/database'
import { authenticationMiddleware } from '../../middleware/authenticationMiddleware'
import { SearchTransitImagesResponse } from './SearchTransitImagesResponse'

const search = new Router({ prefix: '/search' })

const store = new TransitImageStore(database)

search.use(authenticationMiddleware)

search.get('/', async (ctx) => {
  const query = ctx.query['q']
  const limitString = ctx.query['limit']
  const offsetString = ctx.query['offset']
  const limit = limitString ? parseInt(limitString) : undefined
  const offset = offsetString ? parseInt(offsetString) : undefined
  const result: SearchTransitImagesResponse = {
    transitImagesIds: await store.searchImage(query, limit, offset),
  }
  ctx.body = result
  ctx.status = 200
})

export {
  search,
}
