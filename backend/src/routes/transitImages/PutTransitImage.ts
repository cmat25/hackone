import { GetTransitImage } from './GetTransitImage'

type PutTransitImage = Omit<GetTransitImage, 'lastEdited' | 'lastEditedBy' | 'id'>

export { PutTransitImage }