import { GetTransitImage } from '../GetTransitImage'

type GetTransitImageChangesResponse = {
  change_id: number
  change_action: string
  change_author: string
  change_timestamp: string
  after: GetTransitImage
}

export {
  GetTransitImageChangesResponse
}