import * as Router from 'koa-router'
import TransitImageChangeStore from '../../../store/TransitImageChangeStore/TransitImageChangeStore'
import { database } from '../../../database/database'
import { GetTransitImageChangesResponse } from './GetTransitImageChangesResponse'

const changes = new Router()

const changeStore = new TransitImageChangeStore(database)

changes.get('/', async (context) => {
  const author = context.query['author']
  const since = context.query['since']
  const id = context.query['id']
  const changes = await changeStore.changes(since, id, author)
  const result: GetTransitImageChangesResponse[] = changes.map((it) => {
    return {
      change_id: it.change_id,
      change_action: it.change_action,
      change_author: it.change_author,
      change_timestamp: it.change_timestamp,
      after: {
        id: `${it.id}`,
        agencyId: it.agency_id,
        country: it.country_code,
        imageCredit: it.credit,
        isStop: it.is_stop,
        imageReferenceUrl: it.reference_url,
        imageSourceUrl: it.source_url,
        lineName: it.line_name,
        title: it.title,
        position: it.position,
        transitKind: it.transit_kind,
        lastEdited: it.change_timestamp,
        lastEditedBy: it.change_author,
      },
    }
  })

  context.body = result
})

export { changes }