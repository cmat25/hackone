import { Position } from '../../store/common/Position'

type GetTransitImage = {
  id: string
  position?: Position
  agencyId?: string
  imageSourceUrl: string
  imageReferenceUrl: string
  imageCredit: string
  country?: string
  lineName?: string
  transitKind?: string
  isStop: boolean
  title: string
  lastEdited: string
  lastEditedBy: string
}

export { GetTransitImage }