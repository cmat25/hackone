import { GetTransitImage } from './GetTransitImage'

type GetTransitImageResponse = {
  transitImages: GetTransitImage[]
}

export { GetTransitImageResponse }
