import * as Router from 'koa-router'
import { database } from '../../database/database'
import { InsertTransitImageModel, TransitImageModel } from '../../store/TransitImageStore/TransitImageModel'
import { authenticationMiddleware } from '../../middleware/authenticationMiddleware'
import TransitImageStore from '../../store/TransitImageStore/TransitImageStore'
import { GetTransitImage } from './GetTransitImage'
import { PutTransitImage } from './PutTransitImage'
import { GetTransitImageResponse } from './GetTransitImagesResponse'
import { changes } from './changes/changes'

const transitImages = new Router({ prefix: '/transitImages' })

const store = new TransitImageStore(database)

transitImages.use(authenticationMiddleware)

transitImages.use('/changes', changes.routes(), changes.allowedMethods())

transitImages.get('/', async (ctx) => {
  const filterIds = ctx.query['ids']

  const result: GetTransitImageResponse = {
    transitImages: (await store.getImages(filterIds)).map((it) => mapStoreTransitImageToResponse(it)),
  }
  ctx.body = result
  ctx.status = 200
})

transitImages.post('/', async (ctx) => {
  const transitImage: PutTransitImage = ctx.request.body

  // This is a bit dodgy, but it'll have to do until we refactor the tool
  // We want to send save requests from the content backend
  // But we need to record the change against the user signed into the content server, not the 'content server' user account
  const user = ctx.query['user'] || ctx.state.user

  const result = await store.createImage(mapPutTransitImageToInsert(transitImage, user))
  if (result.affectedRows && result.affectedRows > 0) {
    // Insert success!
    const storeImage = (await store.getImages(result.insertId))[0]
    ctx.body = mapStoreTransitImageToResponse(storeImage)
    ctx.status = 200
  } else {
    throw Error(`Error creating Transit Image: ${JSON.stringify(result)}`)
  }
})

transitImages.get('/:id', async (ctx) => {
  const storeImage = (await store.getImages(ctx.params['id']))[0]
  const result = mapStoreTransitImageToResponse(storeImage)
  ctx.body = result
  ctx.status = 200
})


transitImages.put('/:id', async (ctx) => {
  const transitImage: PutTransitImage = ctx.request.body

  // This is a bit dodgy, but it'll have to do until we refactor the tool
  // We want to send save requests from the content backend
  // But we need to record the change against the user signed into the content server, not the 'content server' user account
  const user = ctx.query['user'] || ctx.state.user

  await store.updateImage(ctx.params['id'], mapPutTransitImageToInsert(transitImage, user))

  // TODO: Extract into a separate function because this is identical to GET /:id
  const storeImage = (await store.getImages(ctx.params['id']))[0]
  ctx.body = mapStoreTransitImageToResponse(storeImage)
  ctx.status = 200
})

transitImages.delete('/:id', async (ctx) => {
  const user = ctx.query['user'] || ctx.state.user
  await store.deleteImage(ctx.params['id'], user)
  ctx.status = 204
})


const mapPutTransitImageToInsert = (it: PutTransitImage, user: string): InsertTransitImageModel => {
  return {
    agency_id: it.agencyId,
    country_code: it.country,
    credit: it.imageCredit,
    position: it.position,
    is_stop: it.isStop,
    line_name: it.lineName,
    title: it.title,
    transit_kind: it.transitKind,
    reference_url: it.imageReferenceUrl,
    source_url: it.imageSourceUrl,
    metadata_last_edited_by: user,
  }
}

const mapStoreTransitImageToResponse = (it: TransitImageModel): GetTransitImage => {
  return {
    id: `${it.id}`,
    agencyId: it.agency_id,
    position: it.position,
    imageReferenceUrl: it.reference_url,
    imageSourceUrl: it.source_url,
    isStop: it.is_stop,
    lineName: it.line_name,
    title: it.title,
    transitKind: it.transit_kind,
    country: it.country_code,
    imageCredit: it.credit,
    lastEdited: it.metadata_last_edited,
    lastEditedBy: it.metadata_last_edited_by,
  }
}


export { transitImages }
