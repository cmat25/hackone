import { Middleware } from 'koa'
import { UserStore } from '../store/UserStore/UserStore'

const userStore = new UserStore()

const authenticationMiddleware: Middleware = async (ctx, next) => {
  const encodedUserInfo = ctx.headers['x-endpoint-api-userinfo']

  if (encodedUserInfo) {
    const buff = Buffer.from(encodedUserInfo, 'base64')
    const userInfoString = buff.toString()
    const userInfo = JSON.parse(userInfoString)
    ctx.state.user = userInfo.email
  }

  // If we're not in production we don't have Cloud Endpoints in-front of us
  if (process.env.NODE_ENV !== 'production' || !ctx.state.user) {
    ctx.state.user = 'dev@rome2rio.com'
  }

  if (!userStore.isAuthorised(ctx.state.user)) {
    console.log(`User Forbidden: ${ctx.state.user}.`)
    ctx.status = 403
    ctx.body = {
      success: false,
      message: 'Forbidden',
    }
  } else {
    await next()
  }
}

export { authenticationMiddleware }