import { Middleware } from 'koa'

const errorHandlingMiddleware: Middleware = async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    console.log(`Unhandled error: ${err.toString()}`)
    ctx.status = 500
    ctx.body = {
      success: false,
      message: err.message,
      stacktrace: err.stack,
    }
  }
}

export { errorHandlingMiddleware }
