import { TypeCast } from 'mysql'
import { Position } from '../store/common/Position'

// TypeCast has some strict rules about it's implementation, so be careful
// about changing the implementation
// See: https://github.com/mysqljs/mysql#type-casting
const typeConverter: TypeCast = (field, next) => {
  // This is a bit gross, we have custom parsing type logic that applies to
  // all tables, so, for now, all TINYINT types will be parsed as booleans.
  if (field.type === 'TINY' && field.length === 1) {
    console.log(`converting column to boolean: ${field.db}.${field.table}.${field.name}`)
    return (field.string() === '1') // 1 = true, 0 = false
  } else if (field.name === 'position' && field.type === 'GEOMETRY') {
    console.log(`converting geometry to position: ${field.db}.${field.table}.${field.name}: ${field.type}`)
    const geometry = field.geometry()
    if (geometry) {
      const result: Position = {
        lat: geometry.x,
        lng: geometry.y,
      }
      return result
    } else {
      return null
    }
  } else {
    return next()
  }
}

export { typeConverter }