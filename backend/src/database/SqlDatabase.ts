import { Pool, Query } from 'mysql'
import SqlConnection from './SqlConnection'

/**
 * This is a wrapper around a mysql connection pool that exposes a Promise-like
 * API so that we can use async await instead of callbacks.
 *
 * It provides utility methods to automatically handle connection/transaction
 * lifecycles (withConnection, withTransaction).
 */
class SqlDatabase {
  connectionPool: Pool

  constructor(connectionPool: Pool) {
    this.connectionPool = connectionPool
  }

  executeQuery(options: string, values: any): Promise<any> {
    return new Promise<Query>(((resolve, reject) => {
      this.connectionPool.query(options, values, (err, results, fields) => {
        if (err) {
          reject(err)
        } else {
          resolve(results)
        }
      })
    }))
  }

  private getConnection(): Promise<SqlConnection> {
    return new Promise<SqlConnection>((resolve, reject) => {
      this.connectionPool.getConnection((err, connection) => {
        if (err) {
          reject(err)
        }
        resolve(new SqlConnection(connection))
      })
    })
  }

  async withConnection(body: (connection: SqlConnection) => Promise<void>) {
    const connection = await this.getConnection()
    await body(connection)
    connection.release()
  }

  async withTransaction(body: (connection: SqlConnection) => Promise<void>) {
    this.withConnection(async connection => {
      await connection.withTransaction(async () => {
        await body(connection)
      })
    })
  }
}

export default SqlDatabase
