import * as mysql from 'mysql'
import { PoolConfig, Types } from 'mysql'
import SqlDatabase from './SqlDatabase'
import { Position } from '../store/common/Position'
import { typeConverter } from './typeConverter'

const config: PoolConfig = {
  connectionLimit: 10,
  user: process.env.SQL_USER || 'content',
  password: process.env.SQL_PASSWORD || 'test',
  database: process.env.SQL_DATABASE || 'content',
  typeCast: typeConverter,
}

if (
  process.env.INSTANCE_CONNECTION_NAME &&
  process.env.NODE_ENV === 'production'
) {
  config.socketPath = `/cloudsql/${process.env.INSTANCE_CONNECTION_NAME}`
} else {
  config.host = 'database'
}

config.queryFormat = function(query, values) {
  if (!values) return query
  return query.replace(/\?\d*/g, (key: string) => {
    if (key.length === 1) {
      throw Error('Key must have an index suffix. .e.g ?1')
    }

    const index = parseInt(key.slice(1)) - 1
    const value = values[index]
    return this.escape(value)
  })
}


const databasePool = mysql.createPool(config)
const database = new SqlDatabase(databasePool)
export { database }
