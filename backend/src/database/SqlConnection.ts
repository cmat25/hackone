import { PoolConnection, Query } from 'mysql'

class SqlConnection {
  connection: PoolConnection

  constructor(connection: PoolConnection) {
    this.connection = connection
  }

  async withTransaction(body: () => Promise<void>) {
    this.connection.beginTransaction()
    try {
      await body()
      await this.executeCommit()
    } catch (exception) {
      await this.executeRollback()
    }
  }

  release(): void {
    this.connection.release()
  }

  executeQuery(options: string, values: any): Promise<any> {
    return new Promise<Query>(((resolve, reject) => {
      this.connection.query(options, values, (err, results, fields) => {
        if (err) {
          reject(err)
        } else {
          resolve(results)
        }
      })
    }))
  }

  executeCommit(): Promise<void> {
    return new Promise<void>(((resolve, reject) => {
      this.connection.commit(err => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    }))
  }

  executeRollback(): Promise<void> {
    return new Promise<void>(((resolve, reject) => {
      this.connection.rollback(err => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    }))
  }
}

export default SqlConnection