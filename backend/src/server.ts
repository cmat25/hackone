import * as Koa from 'koa'
import * as Router from 'koa-router'
import * as bodyParser from 'koa-bodyparser'
import * as logger from 'koa-logger'
import { transitImages } from './routes/transitImages/transitImages'
import { search } from './routes/search/search'
import * as dotenv from 'dotenv'
import * as cors from '@koa/cors'
import * as compress from 'koa-compress'
import { errorHandlingMiddleware } from './middleware/errorHandlingMiddleware'
import { constants } from 'zlib'


dotenv.config()

const app = new Koa()
const router = new Router()
router.use(transitImages.routes())
router.use(search.routes())

app.use(compress({
  level: constants.Z_BEST_SPEED,
}))
app.use(cors())
app.use(logger())
app.use(bodyParser())
app.use(errorHandlingMiddleware)
app.use(router.routes())

const port = process.env.PORT || 3000
app.listen(port)
