#!/usr/bin/env bash
gcloud auth activate-service-account content-service-build-machine@content-tools-248505.iam.gserviceaccount.com \
    --key-file=content-service-build-service-account.json &&

gcloud builds submit --project content-tools-248505 --config cloudbuild.yaml . &&

gcloud auth revoke content-service-build-machine@content-tools-248505.iam.gserviceaccount.com
