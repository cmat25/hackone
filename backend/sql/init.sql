# Assuming you're logged into the database as root this set of queries will create the content database and assign
# the content user all permission to the database.
#
# Connections to the database should use the content user, not root.
DROP DATABASE IF EXISTS content;
CREATE DATABASE content;

DROP USER IF EXISTS 'content'@'%';

# The user is created implicitly
GRANT ALL PRIVILEGES ON content.*
  TO 'content'@'%' IDENTIFIED BY 'test'
  WITH GRANT OPTION;
