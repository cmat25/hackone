DROP TABLE IF EXISTS transit_image;
DROP TABLE IF EXISTS transit_image_audit;
DROP TABLE IF EXISTS country;

CREATE TABLE IF NOT EXISTS country
(
    iso            CHAR(2)     NOT NULL PRIMARY KEY,
    name           VARCHAR(80) NOT NULL,
    printable_name VARCHAR(80) NOT NULL,
    iso3           CHAR(3),
    numcode        SMALLINT
) CHARSET utf8mb4;

CREATE TABLE transit_image
(
    id                      INT AUTO_INCREMENT PRIMARY KEY,
    position                POINT,
    agency_id               VARCHAR(255),
    line_name               VARCHAR(255),
    transit_kind            ENUM ('None', 'Air', 'Road', 'Rail', 'Water', 'Walk'), # Matches TransitKind in rome2rio-core
    is_stop                 BOOLEAN       NOT NULL,
    title                   VARCHAR(255)  NOT NULL,
    # The longest url in the tsv at the moment is ~500 characters, the longest allowable url is ~2000 characters, 1024
    # seems like a sensible limit for now.
    source_url              VARCHAR(1024) NOT NULL,
    reference_url           VARCHAR(1024) NOT NULL,
    credit                  VARCHAR(255)  NOT NULL,
    country_code            VARCHAR(2) REFERENCES country (iso),
    metadata_last_edited    TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    # last_edited_by is used to populate the audit table change_author
    metadata_last_edited_by VARCHAR(255)  NOT NULL,
    # We don't actually delete transit_images, just make them inactive. This lets us create DELETE audit log entries
    # and it means that we can always revert a delete easily if one is made by mistake. The table will likely not grow
    # that large, and if size becomes a problem we can always go through and trim all that aren't active over a certain
    # age.
    metadata_is_active      BOOLEAN   DEFAULT TRUE
) CHARSET utf8mb4;

# The audit table is updated automatically when the transit_image table is modified.
CREATE TABLE transit_image_audit
(
    change_id        INT AUTO_INCREMENT PRIMARY KEY      NOT NULL,
    change_author    VARCHAR(255)                        NOT NULL,
    change_action    ENUM ('UPDATE', 'CREATE', 'DELETE') NOT NULL,
    change_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    id               INT                                 NOT NULL,
    position         POINT,
    agency_id        VARCHAR(255),
    source_url       VARCHAR(1024)                       NOT NULL,
    reference_url    VARCHAR(1024)                       NOT NULL,
    credit           VARCHAR(255)                        NOT NULL,
    country_code     VARCHAR(2),
    line_name        VARCHAR(255),
    transit_kind     ENUM ('None', 'Air', 'Road', 'Rail', 'Water', 'Walk'), # Matches TransitKind in rome2rio-core
    is_stop          BOOLEAN                             NOT NULL,
    title            VARCHAR(255)                        NOT NULL
) CHARSET utf8mb4;
