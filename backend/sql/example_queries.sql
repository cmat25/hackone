SELECT id,
       ST_AsGeoJSON(position) as position,
       agency_id,
       source_url,
       reference_url,
       title,
       credit,
       metadata_last_edited,
       metadata_last_edited_by,
       country.printable_name AS country_name
FROM transit_image
         LEFT JOIN country ON country_code = country.iso
WHERE metadata_is_active = TRUE
  AND (printable_name LIKE CONCAT('%', 'United', '%')
    OR title LIKE CONCAT('%', 'United', '%')
    OR agency_id LIKE CONCAT('%', 'United', '%'))
ORDER BY CASE
             # exact matches first
             WHEN printable_name LIKE 'United' OR title LIKE 'United' OR agency_id LIKE 'United' THEN 1
             # Prefix match second
             WHEN printable_name LIKE CONCAT('United', '%') OR title LIKE CONCAT('United', '%') OR
                  agency_id LIKE CONCAT('United', '%')
                 THEN 2
             ELSE 6
             END ASC,
         agency_id ASC,
         title ASC;

SELECT id,
       position,
       agency_id,
       source_url,
       reference_url,
       credit,
       country_code,
       metadata_last_edited,
       metadata_last_edited_by,
       metadata_is_active
FROM transit_image;

SELECT *
FROM transit_image;
