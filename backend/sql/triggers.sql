DROP TRIGGER IF EXISTS transit_image_create;
DELIMITER $$
CREATE DEFINER = 'root'@'%'
    TRIGGER transit_image_create
    AFTER INSERT
    ON transit_image
    FOR EACH ROW
BEGIN
    INSERT INTO transit_image_audit (change_author, change_action, id, position, agency_id,
                                     line_name, transit_kind, is_stop, title, source_url, reference_url,
                                     credit, country_code)
    VALUES (NEW.metadata_last_edited_by,
            'CREATE',
            NEW.id,
            NEW.position,
            NEW.agency_id,
            NEW.line_name,
            NEW.transit_kind,
            NEW.is_stop,
            NEW.title,
            NEW.source_url,
            NEW.reference_url,
            NEW.credit,
            NEW.country_code);
END
$$

DROP TRIGGER IF EXISTS transit_image_update$$

CREATE DEFINER = 'root'@'%' TRIGGER transit_image_update
    AFTER UPDATE
    ON transit_image
    FOR EACH ROW
BEGIN
    # 1: 'UPDATE', 2: 'CREATE', 3: 'DELETE'
    DECLARE actionKind ENUM ('UPDATE', 'CREATE', 'DELETE');

    IF NEW.metadata_is_active > 0 THEN
        SET actionKind := 'UPDATE';
    ELSE
        SET actionKind := 'DELETE';
    END IF;

    INSERT INTO transit_image_audit (change_author, change_action, id, position, agency_id, line_name, transit_kind,
                                     is_stop, title, source_url, reference_url,
                                     credit, country_code)
    VALUES (NEW.metadata_last_edited_by,
            actionKind,
            NEW.id,
            NEW.position,
            NEW.agency_id,
            NEW.line_name,
            NEW.transit_kind,
            NEW.is_stop,
            NEW.title,
            NEW.source_url,
            NEW.reference_url,
            NEW.credit,
            NEW.country_code);
END
$$
