## Content Tools Backend
The backend is deployed to Google Cloud, specifically, we use:
* Cloud Run to deploy Docker containers.
* Cloud SQL to provide a managed MySQL database. 
* Cloud Endpoints to provide API management.
